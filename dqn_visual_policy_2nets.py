import torch
import torch.nn as nn
import torch.autograd as autograd
import numpy as np
import torch.nn.functional as F
import time

class DQN(nn.Module): 
    def __init__(self, input_dim, output_dim):
        super(DQN, self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        
        self.convs = nn.Sequential(
            nn.Conv2d(input_dim[0], 32, kernel_size=8, stride=4),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1),
            nn.ReLU()
        )

        self.fc = nn.Sequential(
            nn.Linear(3136, 512),
            nn.ReLU(),
            nn.Linear(512, self.output_dim),
            nn.ReLU()
        )

    def forward(self, state):
        # print(state.shape)
        x = self.convs(state)
        # print(x.shape)
        x = torch.flatten(x, start_dim=1)
        # print(x.shape)
        # exit()
        qvals = torch.softmax(self.fc(x), dim=1)
        # qvals = self.fc(x)
        return qvals
    
class DQNAgent:
    def __init__(self, env, learning_rate=3e-4, gamma=0.99, buffer_size=10000, state_shape=(50,)):
        self.env = env
        self.learning_rate = learning_rate
        self.gamma = gamma

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        # self.device = 'cpu'
        old_shape = env.observation_space.shape
        # input_shape = (old_shape[2], old_shape[0], old_shape[1])
        input_shape = (3, 84, 84)
        print('Create DQN agent with:')
        print(f'device: {self.device}')
        print(f'observation_space: {input_shape}')
        print(f'action_space: {env.action_space.n}')

        self.policy_net = DQN(input_shape, env.action_space.n).to(self.device)
        self.target_net = DQN(input_shape, env.action_space.n).to(self.device)
        self.target_net.load_state_dict(self.policy_net.state_dict())
        self.target_net.eval()

        self.optimizer = torch.optim.RMSprop(self.policy_net.parameters())

        # self.optimizer = torch.optim.Adam(self.policy_net.parameters(), lr=learning_rate)
        self.MSE_loss = nn.MSELoss()

        self.epsilon = 0.95
        # self.epsilon_decay = 0.9995
        # for 1M 
        self.epsilon_decay = 0.9999978

        # self.epsilon_decay = 0.99998
        self.min_epsilon = 0.05

    def get_action(self, state, eps=0.20):
        state = torch.FloatTensor(state).float().unsqueeze(0).to(self.device)
        qvals = self.policy_net.forward(state).cpu().detach().numpy()
        qvals = np.reshape(qvals, (4))

        return qvals

    # on-policy sampling
    def select_action(self, qvals):
        if np.random.randn() < self.epsilon:
            self.epsilon *= self.epsilon_decay
            return np.random.choice(len(qvals), 1, p=qvals)[0]
        else:
            return np.argmax(qvals)

    # epsilon strategy
    # def select_action(self):
    #     if numpy.random.randn() < agent.epsilon:
    #         agent.epsilon *= agent.epsilon_decay
    #         act = env.action_space.sample()
    #     else:
    #         act = numpy.argmax(action)

    #     return act

    def compute_loss(self, states, actions, rewards, next_states, dones):
        # print(actions)
        # print(actions.shape)
        actions = torch.argmax(actions, dim=1)
        # print(actions.shape)
        # print(type(actions))
        curr_Q = self.policy_net.forward(states).gather(1, actions.unsqueeze(1))
        next_Q = self.target_net.forward(next_states)

        # greedy update (Q-learning)
        # next_Q = torch.max(next_Q, 1)[0]


        # epsilon greedy update (SARSA)
        # t1 = time.time()
        next_Q = next_Q.cpu().detach().numpy()
        temp_Q = np.zeros(next_Q.shape[0])
        for i in range(next_Q.shape[0]):
            temp_Q[i] = self.select_action(next_Q[i])
        next_Q = torch.FloatTensor(temp_Q).to(self.device)
        # print(f"select action time: {time.time() - t1}")

        expected_Q = rewards.squeeze(1) + self.gamma * next_Q
        expected_Q = expected_Q.unsqueeze(1)

        # print(curr_Q[0, 0])
        # print(expected_Q[0, 0])
        # print('----------------')

        # print(curr_Q.shape)
        # print(expected_Q.shape)
        # exit()

        loss = self.MSE_loss(curr_Q, expected_Q)
        # loss = F.smooth_l1_loss(curr_Q, expected_Q)
        return loss

    def update(self, states, actions, rewards, next_states, dones):
        states = torch.FloatTensor(states).to(self.device)
        # CartPole
        # states = (states + 3.0)/6.0
        states /= 255.0
        # print(states)

        actions = torch.LongTensor(actions).to(self.device)
        rewards = torch.FloatTensor(rewards).to(self.device)
        next_states = torch.FloatTensor(next_states).to(self.device)
        # CartPole
        # next_states = (next_states + 3.0)/6.0
        next_states /= 255.0

        dones = torch.FloatTensor(dones).to(self.device)
        dones = dones.view(dones.size(0), -1)

        loss = self.compute_loss(states, actions, rewards, next_states, dones)

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        return loss.item()

    def update_target_net(self):
        self.target_net.load_state_dict(self.policy_net.state_dict())

    def save_model(self, name="model_q"):
        torch.save(self.policy_net.state_dict(), name + "_dqn.md5")

    def load_model(self, name="model_q"):
        self.target_net.load_state_dict(torch.load(name + "_dqn.md5"))
        self.policy_net.load_state_dict(torch.load(name + "_dqn.md5"))
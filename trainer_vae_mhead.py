""" Скрипт для обучения агента и параллельного обучения VAE на одном типе преград.
VAE эмулирует буфер агента для последующей смешивания с другим агентом.
"""


from sac_policy import SACAgent
import gym
from env_wrapper import Wrapper, BWpit, RepeatWrapper, BWg, BWstu, BWstapit, BWsta
from buffer import Buffer
from aparam import aparam
# from mhead_vae import Vae
from conv_mhead_vae import Vae
# from conv_mhead_vae import Vae
import vae_config as config
apr = aparam()
import numpy


buffer_maxlen = 100000

def convert_buffer(buffer):
    l = len(buffer)
    states = numpy.zeros((l, 28))
    nstates = numpy.zeros((l, 20))
    actions = numpy.zeros((l, 4))
    rewards = numpy.zeros((l, 1))
    for i in range(l):
        states[i, :14] = buffer[i][0][:14]
        states[i, 14:] = buffer[i][3][:14]
        # next state
        nstates[i, :10] = buffer[i][0][14:]
        nstates[i, 10:] = buffer[i][3][14:]
        # action
        actions[i,] = buffer[i][1]
        # reward
        rewards[i] = buffer[i][2]

    return states, nstates, actions, rewards

vae = Vae(config)

replay_buffer_a = Buffer(buffer_maxlen, load_from="data/pits/ямы.npz")
buffer = replay_buffer_a.buffer
states, nstates, actions, rewards = convert_buffer(buffer)
vae.fit(states, nstates, actions, rewards)

# TESTING
# import matplotlib.pyplot as plt
# def show(true, pred):
#     for sb, sv in zip(true, pred):
#         # yb = states_a[ii, :48]
#         # yv = states_a[ii, :48]
#         x = numpy.arange(len(sv))
#         plt.scatter(x, sb)
#         plt.scatter(x, sv)
#         plt.show()

# states_buffer, actions_buffer, rewards_buffer, next_states_a, dones_a = replay_buffer_a.sample(batch_size=3)
# states_vae, actions_vae, rewards_vae, next_states_a, dones_a = vae.get_data(minimum=9.919896084070205, maximum=23.242458453377182, batch_size=3)

# show(states_buffer, states_vae)
# show(actions_buffer, actions_vae)
# show(rewards_buffer, rewards_vae)
# exit()

vae.save("data/ямы_conv_mvae.h5")
exit()


vae = Vae(config)
replay_buffer_a = Buffer(buffer_maxlen, load_from="data/stairs/лестницы.npz")
buffer = replay_buffer_a.buffer
states, nstates, actions, rewards = convert_buffer(buffer)
vae.fit(states, nstates, actions, rewards)
vae.save("data/лестницы_mvae.h5")


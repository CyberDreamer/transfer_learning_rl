from sac_policy import SACAgent
import gym
import numpy
from env_wrapper import Wrapper, BWpit, RepeatWrapper, BWg, BWstu, BWstapit, BWsta
from buffer import Buffer
from aparam import aparam
apr = aparam()
# import gan_2
import mhead_gan
import vae_config as config

buffer_maxlen = 100000
buffer = Buffer(buffer_maxlen, load_from="data/pits/ямы2.npz")

gan = mhead_gan.Generator()
gan.load("data/mheadgan_pits_gen2_nobalance.h5")

states_b, actions_b, rewards_b, nstates_b, dones_b = buffer.sample(batch_size=5000)
states_v, actions_v, rewards_v, nstates_v, dones_v = gan.get_data(batch_size=256)

import math
from sklearn import preprocessing

scaler = preprocessing.MinMaxScaler()

states_v = scaler.fit_transform(states_v)
states_b = scaler.transform(states_b)

# TESTING
import matplotlib.pyplot as plt
def show(true, pred):
    for sb, sv in zip(true, pred):
        # yb = states_a[ii, :48]
        # yv = states_a[ii, :48]
        x = numpy.arange(len(sv))
        plt.scatter(x, sb)
        plt.scatter(x, sv)
        plt.show()

# show(states_b, states_v)
# show(actions_b, actions_v)
# show(rewards_b, rewards_v)
# exit()

# for sb in states_b:
#     x = numpy.arange(len(sb))
#     plt.scatter(x, sb)
#     plt.show()

min_distances = []
max_distances = []

for sv in states_v:
    min_distance = 100000
    max_distance = 0
    best_item = None

    for sb in states_b:
        distance = 0
        for ii in range(24):
            distance += math.fabs(sv[ii] - sb[ii])
        
        if distance < min_distance:
            min_distance = distance
            best_item = sb

        if distance > max_distance:
            max_distance = distance
    
    # show([best_item], [sv])
    # print(f'min for vae point: {min_distance/24}')
    min_distances += [min_distance/24]
    max_distances += [max_distance/24]

print(f'Mean min distance: {numpy.mean(min_distances)}')
print(f'Mean max distance: {numpy.mean(max_distances)}')
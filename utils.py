import matplotlib.pyplot as plt
import numpy
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans

def show_states(true_state, gen_state):
    x = numpy.arange(len(true_state))
    plt.scatter(x, true_state)
    plt.scatter(x, gen_state)
    plt.show()

class ReplayBuffer:
    """
    A simple FIFO experience replay buffer for SAC agents.
    класс дополнен функциями экспорта и восстановления буфера в/из массива    
    """
    def init(self, obs_dim, act_dim, size):
        self.obs1_buf = numpy.zeros([size, obs_dim], dtype=numpy.float32)
        self.obs2_buf = numpy.zeros([size, obs_dim], dtype=numpy.float32)
        self.acts_buf = numpy.zeros([size, act_dim], dtype=numpy.float32)
        self.rews_buf = numpy.zeros(size, dtype=numpy.float32)
        self.done_buf = numpy.zeros(size, dtype=numpy.float32)
        self.ptr, self.size, self.max_size = 0, 0, size

    def store(self, obs, act, rew, next_obs, done):
        self.obs1_buf[self.ptr] = obs
        self.obs2_buf[self.ptr] = next_obs
        self.acts_buf[self.ptr] = act
        self.rews_buf[self.ptr] = rew
        self.done_buf[self.ptr] = done
        self.ptr = (self.ptr + 1) % self.max_size
        self.size = min(self.size + 1, self.max_size)

    def sample_batch(self, batch_size=32):
        idxs = numpy.random.randint(0, self.size, size=batch_size)
        return dict(obs1=self.obs1_buf[idxs],
                    obs2=self.obs2_buf[idxs],
                    acts=self.acts_buf[idxs],
                    rews=self.rews_buf[idxs],
                    done=self.done_buf[idxs])

    def export_np(self):
        np_arr = numpy.hstack((self.obs1_buf, self.obs2_buf, self.acts_buf, self.rews_buf.reshape(self.rews_buf.shape[0],1)))
        return np_arr[0:self.size]

    def restore(self, arr):
        arrsz = arr.shape[0]
        self.obs1_buf = arr[:,0:24]
        self.obs2_buf = arr[:,24:48]
        self.acts_buf = arr[:,48:52]
        self.rews_buf = arr[:,52]
        self.done_buf = numpy.zeros(arrsz)
        self.ptr = 0
        self.size = arrsz
        self.max_size = arrsz

def dbalance(data_np, ndf=10, eps=0.2, nsl=5, nst = -1, in_col = 14, out_col = 24):
    """
    Принимает на вход репл буффер и возвращает его прореженную в ndf раз версию
    """
    # data_np = repbuf.export_np()
    # repbuf = ReplayBuffer(24, 4, int(2e6))
    # db = DBSCAN(eps=eps, min_samples=nsl)
    clstr = KMeans(n_clusters=2)
    # db.fit(data_np[:nst, in_col:out_col])
    clstr.fit(data_np[:nst, in_col:out_col])
    data_cl1 = data_np[numpy.where(clstr.labels_==0)]
    data_cl2 = data_np[numpy.where(clstr.labels_!=0)]

    # for ii in range(len(data_cl1)):
    #     show_states(data_cl1[ii], data_cl2[ii])

    # exit()

    order = numpy.random.randint(0, data_cl1.shape[0], size=data_cl1.shape[0]//ndf)
    data_cl3 = data_cl1[order]
    data_cl0 = numpy.vstack((data_cl3, data_cl2))
    numpy.random.shuffle(data_cl0)
    # repbuf.restore(data_cl0)
    return data_cl0

def train_series(train_count, max_total_steps, train_func):
    all_rewards = numpy.zeros((train_count, max_total_steps))
    all_steps = numpy.zeros((train_count, max_total_steps))

    for exp in range(train_count):
        rewards_log, step_log = train_func(max_episodes=300, max_steps=2000, batch_size=128)
        if len(rewards_log) < max_total_steps:
            all_rewards[exp, :len(rewards_log)] = rewards_log
            all_steps[exp, :len(step_log)] = step_log
        else:
            all_rewards[exp, :] = rewards_log[:max_total_steps]
            all_steps[exp, :] = step_log[:max_total_steps]

    return all_rewards, all_steps

def plot_rl_statisctic(all_rewards, max_total_steps):
    means = numpy.mean(all_rewards, axis=0)
    trees_grid = numpy.linspace(0, max_total_steps, max_total_steps)
    plt.style.use('ggplot')

    fig, ax = plt.subplots(figsize=(8, 4))
    # ax.plot(trees_grid, train_acc.mean(axis=1), alpha=0.5, color='blue', label='train', linewidth = 4.0)
    ax.plot(trees_grid, means, alpha=0.5, color='red', label='on real env', linewidth = 1.0)
    ax.fill_between(trees_grid, means - all_rewards.std(axis=0), means + all_rewards.std(axis=0), color='#888888', alpha=0.4)
    ax.fill_between(trees_grid, means - 2*all_rewards.std(axis=0), means + 2*all_rewards.std(axis=0), color='#888888', alpha=0.2)
    ax.legend(loc='best')
    ax.set_ylabel("Rewards")
    ax.set_xlabel("Steps")
    plt.show()
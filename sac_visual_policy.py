import torch
import torch.nn.functional as F
import torch.optim as optim
from torch.distributions import Normal
import numpy
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Normal, Uniform


class ValueNetwork(nn.Module):
    def __init__(self, input_dim, output_dim, init_w=3e-3):
        super(ValueNetwork, self).__init__()
        self.fc1 = nn.Linear(input_dim, 256)
        self.fc2 = nn.Linear(256, 256)
        self.fc3 = nn.Linear(256, output_dim)

        self.fc3.weight.data.uniform_(-init_w, init_w)
        self.fc3.bias.data.uniform_(-init_w, init_w)

    def forward(self, state):
        x = F.relu(self.fc1(state))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)

        return x


class SoftQNetwork(nn.Module):
    def __init__(self, input_shape, num_actions, hidden_size=256, init_w=3e-3):
        super(SoftQNetwork, self).__init__()
        self.convs = nn.Sequential(
            nn.Conv2d(input_shape[0], 32, kernel_size=8, stride=4),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1),
            nn.ReLU()
        )

        # self.linear1 = nn.Linear(num_inputs + num_actions, hidden_size)
        # self.linear2 = nn.Linear(hidden_size, hidden_size)
        # self.linear3 = nn.Linear(hidden_size, 1)

        # self.linear3.weight.data.uniform_(-init_w, init_w)
        # self.linear3.bias.data.uniform_(-init_w, init_w)

    def forward(self, state, action):
        state = state.unsqueeze(2)
        action = action.unsqueeze(2)

        # exit()

        state = self.convs(state)
        print(state.shape)
        print(action.shape)
        exit()

        x = torch.cat((state, action), 1)
        x = torch.squeeze(x, 2)
        # print(x.shape)
        # exit()
        # x = torch.stack([state, action], 1)
        x = F.relu(self.linear1(x))
        x = F.relu(self.linear2(x))
        x = self.linear3(x)
        return x


class GaussianPolicy(nn.Module):
    def __init__(self, num_inputs, num_actions, hidden_size=256, init_w=3e-3, log_std_min=-20, log_std_max=2):
        super(GaussianPolicy, self).__init__()
        self.log_std_min = log_std_min
        self.log_std_max = log_std_max

        self.linear1 = nn.Linear(num_inputs, hidden_size)
        self.linear2 = nn.Linear(hidden_size, hidden_size)

        self.mean_linear = nn.Linear(hidden_size, num_actions)
        self.mean_linear.weight.data.uniform_(-init_w, init_w)
        self.mean_linear.bias.data.uniform_(-init_w, init_w)

        self.log_std_linear = nn.Linear(hidden_size, num_actions)
        self.log_std_linear.weight.data.uniform_(-init_w, init_w)
        self.log_std_linear.bias.data.uniform_(-init_w, init_w)

    def forward(self, state):
        x = F.relu(self.linear1(state))
        x = F.relu(self.linear2(x))

        mean    = self.mean_linear(x)
        log_std = self.log_std_linear(x)
        log_std = torch.clamp(log_std, self.log_std_min, self.log_std_max)

        return mean, log_std

    def sample(self, state, epsilon=1e-6):
        mean, log_std = self.forward(state)
        std = log_std.exp()
        normal = Normal(mean, std)
        z = normal.rsample()
        log_pi = (normal.log_prob(z) - torch.log(1 - (torch.tanh(z)).pow(2) + epsilon)).sum(1, keepdim=True)

        return mean, std, z, log_pi

class SACAgent:
    def __init__(self, env, gamma, tau, alpha, q_lr, policy_lr, a_lr, buffer_maxlen):
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        self.env = env
        # print(env.action_space)
        # exit()
        # self.action_range = [env.action_space.low, env.action_space.high]
        # self.obs_dim = env.observation_space.shape
        self.obs_dim = (4, 84, 84)
        # self.action_dim = env.action_space.shape[0]
        # for CartPole
        print(f'action_space - {env.action_space.n}')
        self.action_dim = env.action_space.n

        print(f'SAC Agent created with: ')
        print(f'observation_space - {self.obs_dim}')
        print(f'action_space - {self.action_dim}')

        # hyperparameters
        self.gamma = gamma
        self.tau = tau

        # initialize networks
        self.q_net1 = SoftQNetwork(self.obs_dim, self.action_dim).to(self.device)
        self.q_net2 = SoftQNetwork(self.obs_dim, self.action_dim).to(self.device)
        self.target_q_net1 = SoftQNetwork(self.obs_dim, self.action_dim).to(self.device)
        self.target_q_net2 = SoftQNetwork(self.obs_dim, self.action_dim).to(self.device)
        self.policy_net = GaussianPolicy(self.obs_dim, self.action_dim).to(self.device)

        # copy params to target param
        for target_param, param in zip(self.target_q_net1.parameters(), self.q_net1.parameters()):
            target_param.data.copy_(param)

        for target_param, param in zip(self.target_q_net2.parameters(), self.q_net2.parameters()):
            target_param.data.copy_(param)

        # initialize optimizers
        self.q1_optimizer = optim.Adam(self.q_net1.parameters(), lr=q_lr)
        self.q2_optimizer = optim.Adam(self.q_net2.parameters(), lr=q_lr)
        self.policy_optimizer = optim.Adam(self.policy_net.parameters(), lr=policy_lr)

        # entropy temperature
        self.alpha = alpha
        self.target_entropy = -torch.prod(torch.Tensor(self.env.action_space.shape).to(self.device)).item()
        self.log_alpha = torch.zeros(1, requires_grad=True, device=self.device)
        self.alpha_optim = optim.Adam([self.log_alpha], lr=a_lr)

        # self.epsilon = 0.95
        # self.epsilon_decay = 0.995

    def get_action(self, state):
        state = torch.FloatTensor(state).unsqueeze(0).to(self.device)
        mean, log_std = self.policy_net.forward(state)
        std = log_std.exp()

        normal = Normal(mean, std)
        z = normal.sample()
        # print(z)
        # exit()
        action = torch.tanh(z)
        action = action.cpu().detach().squeeze(0).numpy()
        # action = numpy.argmax(action)

        return action

    def update(self, states, actions, rewards, next_states, dones):
        states = torch.FloatTensor(states).to(self.device)
        actions = torch.FloatTensor(actions).to(self.device)
        rewards = torch.FloatTensor(rewards).to(self.device)
        next_states = torch.FloatTensor(next_states).to(self.device)
        dones = torch.FloatTensor(dones).to(self.device)
        dones = dones.view(dones.size(0), -1)

        _, _, next_zs, next_log_pi = self.policy_net.sample(next_states)
        next_actions = torch.tanh(next_zs)
        next_q1 = self.target_q_net1(next_states, next_actions)
        next_q2 = self.target_q_net2(next_states, next_actions)
        next_q_target = torch.min(next_q1, next_q2) - self.alpha * next_log_pi
        expected_q = rewards + (1 - dones) * self.gamma * next_q_target

        # q loss
        curr_q1 = self.q_net1.forward(states, actions)
        curr_q2 = self.q_net2.forward(states, actions)
        q1_loss = F.mse_loss(curr_q1, expected_q.detach())
        q2_loss = F.mse_loss(curr_q2, expected_q.detach())

        # update q networks
        self.q1_optimizer.zero_grad()
        q1_loss.backward()
        self.q1_optimizer.step()

        self.q2_optimizer.zero_grad()
        q2_loss.backward()
        self.q2_optimizer.step()

        # delayed update for policy network and target q networks
        _, _, new_zs, log_pi = self.policy_net.sample(states)
        new_actions = torch.tanh(new_zs)
        min_q = torch.min(
            self.q_net1.forward(states, new_actions),
            self.q_net2.forward(states, new_actions)
        )
        policy_loss = (self.alpha * log_pi - min_q).mean()
        self.policy_optimizer.zero_grad()
        policy_loss.backward()
        self.policy_optimizer.step()

        # target networks
        for target_param, param in zip(self.target_q_net1.parameters(), self.q_net1.parameters()):
            target_param.data.copy_(self.tau * param + (1 - self.tau) * target_param)

        for target_param, param in zip(self.target_q_net2.parameters(), self.q_net2.parameters()):
            target_param.data.copy_(self.tau * param + (1 - self.tau) * target_param)

        # update temperature
        alpha_loss = (self.log_alpha * (-log_pi - self.target_entropy).detach()).mean()

        self.alpha_optim.zero_grad()
        alpha_loss.backward()
        self.alpha_optim.step()
        self.alpha = self.log_alpha.exp()

    def save_model(self, name="model_q"):
        torch.save(self.q_net1.state_dict(), name + "_q_net1.md5")
        torch.save(self.q_net2.state_dict(), name + "_q_net2.md5")
        torch.save(self.target_q_net1.state_dict(), name + "_target_q_net1.md5")
        torch.save(self.target_q_net2.state_dict(), name + "_target_q_net2.md5")
        torch.save(self.policy_net.state_dict(), name + "_policy_net.md5")

    def load_model(self, name="model_q"):
        self.q_net1.load_state_dict(torch.load(name + "_q_net1.md5"))
        self.q_net1.eval()

        self.q_net2.load_state_dict(torch.load(name + "_q_net2.md5"))
        self.q_net2.eval()

        self.target_q_net1.load_state_dict(torch.load(name + "_target_q_net1.md5"))
        self.target_q_net1.eval()

        self.target_q_net2.load_state_dict(torch.load(name + "_target_q_net2.md5"))
        self.target_q_net2.eval()

        self.policy_net.load_state_dict(torch.load(name + "_policy_net.md5"))
        self.policy_net.eval()

    
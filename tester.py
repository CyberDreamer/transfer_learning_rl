"""
Тестирует базового агента (который умеет проходить только один вид преград)
"""
from sac_policy import SACAgent
import gym
from env_wrapper import Wrapper, BWpit, RepeatWrapper, BWg, BWstu, BWstapit, BWsta
from buffer import Buffer
from aparam import aparam
apr = aparam()


# env = gym.make("BipedalWalker-v2")
# env = RepeatWrapper(BWg(), apr, 3)
# env = RepeatWrapper(BWpit(), apr, 3)
env = RepeatWrapper(BWstapit(), apr, 3)
# env = RepeatWrapper(BWsta(), apr, 1)

gamma = 0.99
tau = 0.01
alpha = 0.2
a_lr = 3e-4
q_lr = 1e-3
p_lr = 1e-3
buffer_maxlen = 200000

agent = SACAgent(env, gamma, tau, alpha, q_lr, p_lr, a_lr, buffer_maxlen)
# agent.load_model(name="data/stairs/лестницы")
# agent.load_model(name="data/buffer_mix_stairs_pits/лестницы_ямы_buffer")
agent.load_model(name="data/vae_mix_stairs_pits/vae_mix")

# replay_buffer_a = Buffer(buffer_maxlen, load_from="ямы.npz")

def rescale_action(action):
        return action * (agent.action_range[1] - agent.action_range[0]) / 2.0 +\
            (agent.action_range[1] + agent.action_range[0]) / 2.0

def test(env, agent, max_episodes, max_steps):
    for episode in range(max_episodes):
        state = env.reset()
        episode_reward = 0

        for step in range(max_steps):
            env.render()
            action = agent.get_action(state)
            next_state, reward, done, _ = env.step(rescale_action(action))
            episode_reward += reward

            if done or step == max_steps-1:
                print("Episode " + str(episode) + ": " + str(episode_reward))
                break

            state = next_state

test(env, agent, 50, 5000)

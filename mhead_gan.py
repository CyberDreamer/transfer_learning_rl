# example of a dcgan on cifar10
import numpy
from numpy import expand_dims
from numpy import zeros
from numpy import ones
from numpy import vstack
from numpy.random import randn
from numpy.random import randint
from keras.datasets.cifar10 import load_data
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Dense, Input, Concatenate
from keras.layers import Reshape
from keras.layers import Flatten
from keras.layers import Conv2D
from keras.layers import Conv2DTranspose
from keras.layers import LeakyReLU
from keras.layers import Dropout
from matplotlib import pyplot
from buffer import Buffer
from keras.models import Model
from keras import backend


# size of the latent space
latent_dim = 20



def wasserstein_loss(y_true, y_pred):
    return backend.mean(y_true * y_pred)

# implementation of wasserstein loss
def d_loss(d_real_logit, d_fake_logit):
    return backend.mean(d_real_logit - d_fake_logit)

def g_loss(d_real_logit, d_fake_logit):
    return backend.mean(d_fake_logit)

# define the standalone discriminator model
def define_discriminator(in_shape=(53,)):
    # model = Sequential()
    input_state = Input(shape=(28,), name='input_state')
    input_nstate = Input(shape=(20,), name='input_nstate')
    input_action = Input(shape=(4,), name='input_action')
    input_reward = Input(shape=(1,), name='input_reward')
    xa1 = Dense(32,activation='relu')(input_state)
    xb1 = Dense(32,activation='relu')(input_nstate)
    xc1 = Dense(16,activation='relu')(input_action)
    xd1 = Dense(4,activation='relu')(input_reward)

    xa2 = Dense(64,activation='relu')(xa1)
    xb2 = Dense(64,activation='relu')(xb1)
    xc2 = Dense(32,activation='relu')(xc1)
    xd2 = Dense(8,activation='relu')(xd1)

    combined = Concatenate(axis=1)([xa2, xb2, xc2, xd2])

    z1 = Dense(32, activation='relu')(combined)
    output = Dense(1, activation='relu')(z1)
    model = Model([input_state, input_nstate, input_action, input_reward], output, name='discriminator')
    # compile model
    opt = Adam(lr=0.0002, beta_1=0.5)
    # opt = Adam(lr=0.0001)
    model.compile(loss=wasserstein_loss, optimizer=opt, metrics=['accuracy'])
    return model

class Generator():
    def __init__(self):
        super().__init__()
        # self.model = Sequential()
        # foundation for 4x4 image
        # n_nodes = 256 * 4 * 4
        input_state = Input(shape=(latent_dim,))
        h1 = Dense(32, activation='relu')(input_state)
        h2 = Dense(64, activation='relu')(h1)

        ya1 = Dense(128, activation='relu')(h2)
        yb1 = Dense(128, activation='relu')(h2)
        yc1 = Dense(32, activation='relu')(h2)
        yd1 = Dense(16, activation='relu')(h2)

        output_state = Dense(28, activation='relu')(ya1)
        output_nstate = Dense(20, activation='relu')(yb1)
        output_action = Dense(4, activation='relu')(yc1)
        output_reward = Dense(1, activation='relu')(yd1)

        self.model = Model(input_state, [output_state, output_nstate, output_action, output_reward], name='generator')

    def unnorm(self, data, min, max):
        return data * max - min - 1

    """ для ям на 100 тыс. - mins=[4.638, 1.060, 2.0, 11.972], maxs=[8.281, 2.060, 3.0, 25.717]
        для лестниц на 100 тыс. - mins=[], maxs=[]
    """
    def get_data(self, mins=[4.638, 1.060, 2.0, 11.972], maxs=[8.281, 2.060, 3.0, 25.717], batch_size=128):
        z_sample = numpy.random.randn(batch_size, latent_dim)
        states, nstates, actions, rewards = self.model.predict(z_sample)

        # states = self.unnorm(states, mins[0], maxs[0])
        # nstates = self.unnorm(nstates, mins[1], maxs[1])
        # actions = self.unnorm(actions, mins[2], maxs[2])
        # rewards = self.unnorm(rewards, mins[3], maxs[3])

        obs1 = numpy.zeros((states.shape[0], 24))
        obs2 = numpy.zeros((states.shape[0], 24))
        obs1[:, :14] = states[:, :14]
        obs1[:, 14:] = nstates[:, :10]
        obs2[:, :14] = states[:, 14:]
        obs2[:, 14:] = nstates[:, 10:]
        d = numpy.zeros((batch_size))
        return obs1, actions, rewards, obs2, d

    def save(self, name="gan_g.h5"):
        self.model.save_weights(name)

    def load(self, name="gan_g.h5"):
        self.model.load_weights(name)

# define the combined generator and discriminator model, for updating the generator
def define_gan(g_model, d_model):
    # make weights in the discriminator not trainable
    d_model.trainable = False
    # connect them
    # model = Sequential()
    # add generator
    # model.add(g_model)
    # add the discriminator
    # model.add(d_model)
    gan_input = Input(shape=(latent_dim,))
    H = g_model(gan_input)
    gan_V = d_model(H)

    model = Model(gan_input, gan_V, name='vae')
    # compile model
    opt = Adam(lr=0.0002, beta_1=0.5)
    # opt = Adam(lr=0.0001)
    model.compile(loss=wasserstein_loss, optimizer=opt)
    return model

# select real samples
def generate_real_samples(n_samples):
    # choose random instances
    ix = randint(0, states.shape[0], n_samples)
    # retrieve selected images
    # X = replay_buffer_a[ix]
    x1 = states[ix]
    x2 = nstates[ix]
    x3 = actions[ix]
    x4 = rewards[ix]

    # states_a, actions_a, rewards_a, next_states_a, dones_a = replay_buffer_a.sample(batch_size=n_samples)

    # generate 'real' class labels (1)
    y = -ones((n_samples, 1))
    # y = d_model.predict(X)
    return x1, x2, x3, x4, y

# generate points in latent space as input for the generator
def generate_latent_points(latent_dim, n_samples):
    # generate points in the latent space
    x_input = randn(latent_dim * n_samples)
    # reshape into a batch of inputs for the network
    x_input = x_input.reshape(n_samples, latent_dim)
    return x_input

# use the generator to generate n fake examples, with class labels
def generate_fake_samples(g_model, latent_dim, n_samples):
    # generate points in latent space
    x_input = generate_latent_points(latent_dim, n_samples)
    # predict outputs
    x1, x2, x3, x4 = g_model.predict(x_input)
    # create 'fake' class labels (0)
    y = ones((n_samples, 1))
    # y = d_model.predict(X)
    return x1, x2, x3, x4, y

# create and save a plot of generated images
def save_plot(examples, epoch, n=7):
    # scale from [-1,1] to [0,1]
    examples = (examples + 1) / 2.0
    # plot images
    for i in range(n * n):
        # define subplot
        pyplot.subplot(n, n, 1 + i)
        # turn off axis
        pyplot.axis('off')
        # plot raw pixel data
        pyplot.imshow(examples[i])
    # save plot to file
    filename = 'generated_plot_e%03d.png' % (epoch+1)
    pyplot.savefig(filename)
    pyplot.close()

# evaluate the discriminator, plot generated images, save generator model
def summarize_performance(epoch, g_model, d_model, latent_dim, n_samples=150):
    print(f'Epoch: {epoch + 1}')
    g_model.save("data/mheadgan_pits_gen2_nobalance.h5")

# train the generator and discriminator
def train(g_model, d_model, gan_model, latent_dim, n_epochs=40, n_batch=256):
    bat_per_epo = 756
    # bat_per_epo = 1024
    half_batch = int(n_batch / 2)
    # manually enumerate epochs
    for i in range(n_epochs):
        # enumerate batches over the training set
        for j in range(bat_per_epo):
            # get randomly selected 'real' samples
            x1, x2, x3, x4, y_real = generate_real_samples(half_batch)
            # print(X_real)
            # exit()
            # update discriminator model weights
            d_loss1, _ = d_model.train_on_batch([x1, x2, x3, x4], y_real)
            # generate 'fake' examples
            x1, x2, x3, x4, y_fake = generate_fake_samples(g_model, latent_dim, half_batch)
            # print(X_fake)
            # exit()
            # update discriminator model weights
            d_loss2, _ = d_model.train_on_batch([x1, x2, x3, x4], y_fake)
            # prepare points in latent space as input for the generator
            X_gan = generate_latent_points(latent_dim, n_batch)
            # create inverted labels for the fake samples
            y_gan = -ones((n_batch, 1))
            # update the generator via the discriminator's error
            g_loss = gan_model.train_on_batch(X_gan, y_gan)

            # Clip critic weights
            for l in d_model.layers:
                weights = l.get_weights()
                weights = [numpy.clip(w, -0.01, 0.01) for w in weights]
                l.set_weights(weights)
                    
            # summarize loss on this batch
            # print('>%d, %d/%d, d1=%.3f, d2=%.3f g=%.3f' %
            #     (i+1, j+1, bat_per_epo, d_loss1, d_loss2, g_loss))
        # evaluate the model performance, sometimes
        if (i+1) % 10 == 0:
            summarize_performance(i, g_model, d_model, latent_dim)

def bufer2numpy(buffer):
    l = len(buffer)
    numpy_buffer = numpy.zeros((l, 54))
    for i in range(l):
        numpy_buffer[i, :24] = buffer[i][0]
        # next state
        numpy_buffer[i, 24:48] = buffer[i][3]
        # action
        numpy_buffer[i, 48:52] = buffer[i][1]
        # reward
        numpy_buffer[i, 52] = buffer[i][2]

    return numpy_buffer

def unstack(buffer):
    l = len(buffer)
    states = numpy.zeros((l, 28))
    nstates = numpy.zeros((l, 20))
    actions = numpy.zeros((l, 4))
    rewards = numpy.zeros((l, 1))
    for i in range(l):
        states[i, :14] = buffer[i][:14]
        states[i, 14:] = buffer[i][24:38]
        # next state
        nstates[i, :10] = buffer[i][14:24]
        nstates[i, 10:] = buffer[i][38:48]
        # action
        actions[i,] = buffer[i][48:52]
        # reward
        rewards[i] = buffer[i][52]

    return states, nstates, actions, rewards

def convert_buffer(buffer):
    l = len(buffer)
    states = numpy.zeros((l, 28))
    nstates = numpy.zeros((l, 20))
    actions = numpy.zeros((l, 4))
    rewards = numpy.zeros((l, 1))
    for i in range(l):
        states[i, :14] = buffer[i][0][:14]
        states[i, 14:] = buffer[i][3][:14]
        # next state
        nstates[i, :10] = buffer[i][0][14:]
        nstates[i, 10:] = buffer[i][3][14:]
        # action
        actions[i,] = buffer[i][1]
        # reward
        rewards[i] = buffer[i][2]

    return states, nstates, actions, rewards

def norm(data):
    min = abs(numpy.min(data)) + 1
    data += min
    max = numpy.max(data)
    data /= max

    print(f'min: {min}, max: {max}')
    return data

if __name__ == "__main__":
    buffer_maxlen = 100000
    replay_buffer_a = Buffer(buffer_maxlen, load_from="data/pits/ямы2.npz")
    stacked_buffer = bufer2numpy(replay_buffer_a.buffer)
    # print(stacked_buffer.shape)
    
    import utils
    balanced_buffer = utils.dbalance(stacked_buffer)
    states, nstates, actions, rewards = unstack(balanced_buffer)

    # print(states.shape)
    # exit()

    # states = norm(states)
    # nstates = norm(nstates)
    # actions = norm(actions)
    # rewards = norm(rewards)

    # create the discriminator
    d_model = define_discriminator()
    # create the generator
    g_model = Generator().model
    # g_model.load_weights("data/mheadgan_pits_gen.h5")
    # create the gan
    gan_model = define_gan(g_model, d_model)
    # load image data
    # dataset = load_real_samples()
    # train model
    train(g_model, d_model, gan_model, latent_dim)
from keras.layers import Lambda, Input, Dense
from keras.models import Model
from keras.losses import binary_crossentropy, mse
from keras import regularizers
from keras import backend as K
from keras.callbacks import EarlyStopping
import tensorflow as tf
import keras
# from keras import regularizers
# from utils import normalize_vae, reverse_normalize_vae

import numpy as np

class Vae:
    def __init__(self, config):
        self.config = config
        self.models, loss = self.create_vae()
        self.vae = self.models['vae']
        self.vae.compile(optimizer='adam', loss=loss)
        self.encoder = self.models['encoder']
        self.decoder = self.models['decoder']

    def sampling(self, args):
        """Reparameterization trick by sampling from an isotropic unit Gaussian.
        # Arguments
            args (tensor): mean and log of variance of Q(z|X)
        # Returns
            z (tensor): sampled latent vector
        """

        z_mean, z_log_var = args
        batch = K.shape(z_mean)[0]
        dim = K.int_shape(z_mean)[1]
        # by default, random_normal has mean = 0 and std = 1.0
        epsilon = K.random_normal(shape=(batch, dim))
        return z_mean + K.exp(0.5 * z_log_var) * epsilon


    def create_vae(self):
        original_dim = self.config.ORIGINAL_DIM
        latent_dim = self.config.LATENT_DIM
        structure_encoder = self.config.STRUCTURE_ENCODER
        structure_decoder = self.config.STRUCTURE_DECODER
        models = {}
        inputs = Input(shape=(original_dim,), name='encoder_input')
        # Убрали регуляризацию, т.к. хотели "словить" хотя бы переобучение. В принципе ничего не мешает вернуть регуляризацию
        # Добавили больше слоев, чтобы находить более сложные признаки
        x1 = Dense(64,activation='relu', kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4))(inputs)
        # x2 = Dense(128,activation='relu')(x1)
        # x3 = Dense(128,activation='relu')(x2)
        x4 = Dense(256,activation='relu', kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4))(x1)
        z_mean = Dense(latent_dim, name='z_mean', kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4))(x4)
        z_log_var = Dense(latent_dim, name='z_log_var', kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4))(x4)

        z = Lambda(self.sampling, output_shape=(latent_dim,), name='z')([z_mean, z_log_var])

        # instantiate encoder model
        encoder = Model(inputs, [z_mean, z_log_var, z], name='encoder')
        models["encoder"] = encoder

        # build decoder model
        latent_inputs = Input(shape=(latent_dim,), name='z_sampling')
        # Добавили больше слоев, чтобы находить более сложные признаки
        x1 = Dense(256, activation='relu', kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4))(latent_inputs)
        # x2 = Dense(128, activation='relu')(x1)
        # x3 = Dense(128, activation='relu')(x2)
        x4 = Dense(64, activation='relu', kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4))(x1)
        # сменили функцию активации с sigmoid на relu, так ошибка стала падать сильнее
        outputs = Dense(original_dim, activation='relu', kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4))(x4)

        # instantiate decoder model
        models["decoder"] = Model(latent_inputs, outputs, name='decoder')

        # instantiate VAE model
        outputs = models["decoder"](encoder(inputs)[2])
        models["vae"] = Model(inputs, outputs, name='vae')
        
        # def vae_loss(y_true,y_pred):
        #     # изменили аргументы loss функции с inputs, outputs на локальные значения в батче y_true, y_pred
        #     reconstruction_loss = binary_crossentropy(y_true,y_pred)
        #     # reconstruction_loss *= original_dim
        #     kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
        #     kl_loss = K.sum(kl_loss, axis=-1)
        #     kl_loss *= -0.5
        #     return K.mean(reconstruction_loss + kl_loss)

        def vae_loss(y_true, y_pred):
            reconstruction_loss = tf.reduce_mean(
                keras.losses.binary_crossentropy(y_true, y_pred)
            )
            reconstruction_loss *= original_dim
            kl_loss = 1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var)
            kl_loss = tf.reduce_mean(kl_loss)
            kl_loss *= -0.5
            total_loss = reconstruction_loss + kl_loss
            return total_loss

        return models, vae_loss

    def fit(self, x_train):
        # x_train, self.minimums, self.maximums = normalize_vae(x_train.copy())

        # self.minimum = abs(np.min(x_train)) + 1
        # x_train += self.minimum
        # self.maximum = np.max(x_train)
        # x_train /= self.maximum

        # print(f'min: {self.minimum}, max: {self.maximum}')
        # exit()

        early_stopping = EarlyStopping(monitor='val_loss', patience=10)
        self.vae.fit(x_train, x_train, epochs=self.config.EPOCHS, batch_size=self.config.BATCH_SIZE,
                     validation_split=self.config.VALIDATION_SPLIT, callbacks=[])
        # self.vae.fit(x_train, x_train, epochs=self.config.EPOCHS, batch_size=self.config.BATCH_SIZE,
        #              validation_split=self.config.VALIDATION_SPLIT)

    def save(self, name="model_vae.h5"):
        self.vae.save_weights(name)

    def load(self, name="model_vae.h5"):
        self.vae.load_weights(name)

    def get_data(self, min, max, batch_size=128):
        z_sample = np.random.randn(batch_size, self.config.LATENT_DIM)
        X_batch = self.decoder.predict(z_sample)
        # X_batch = reverse_normalize_vae(X_batch, self.minimums, self.maximums)[0]
        # X_batch = X_batch * maximum - minimum - 1
        obs1 = X_batch[:, :24]
        obs2 = X_batch[:, 24:48]
        acts = X_batch[:, 48:52]
        rews = X_batch[:, 52].reshape(-1, 1)
        d = np.zeros((batch_size))
        return obs1, acts, rews, obs2, d
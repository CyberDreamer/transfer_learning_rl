"""
Учит базового агента (который умеет проходить только один вид преград)
и сохраняет буфер и обученные веса модели.
"""
from sac_policy import SACAgent
from dqn_policy import DQNAgent
import gym
from env_wrapper import Wrapper, BWpit, RepeatWrapper, BWg, BWstu, BWstapit, BWsta
from buffer import Buffer
from aparam import aparam
apr = aparam()
import cv2
import numpy

# ОБУЧЕНИЕ НА ТРАВЕ НА БАЗОВОЙ СРЕДЕ OPEN AI БЕЗ ОБЕРТОК
# env = gym.make("BipedalWalker-v2")
# model_name = "source_env_трава"
# buffer_name = "source_env_трава.npz"


# ДЛЯ ОБЕРТОК
# env = BWg() - если нужна среда с травой
# env = BWpit() - если нужна среда с ямами
# env = BWstapit() - если нужна среда с и лестницами

# ОБУЧЕНИЕ НА ТРАВЕ НА ОБЕРТКЕ С ПОВТОРЕНИЕМ ДЕЙСТВИЙ БЕЗ ШУМА
# env = RepeatWrapper(BWg(), apr, action_repeat=3)
# model_name = "repeat_env_трава"
# buffer_name = "repeat_env_трава.npz"

# ОБУЧЕНИЕ НА ЯМАХ НА ОБЕРТКЕ С ПОВТОРЕНИЕМ ДЕЙСТВИЙ БЕЗ ШУМА
env = RepeatWrapper(BWpit(), apr, action_repeat=3)
model_name = "data/pits/ямы2"
buffer_name = "data/pits/ямы2.npz"

# ОБУЧЕНИЕ НА ЛЕСТНИЦАХ НА ОБЕРТКЕ С ПОВТОРЕНИЕМ ДЕЙСТВИЙ БЕЗ ШУМА
# env = RepeatWrapper(BWsta(), apr, action_repeat=3)
# model_name = "data/stairs/лестницы_temp"
# buffer_name = "data/stairs/лестницы_temp.npz"


# ОБУЧЕНИЕ НА ТРАВЕ НА ОБЕРТКЕ С ПОВТОРЕНИЕМ ДЕЙСТВИЙ С ШУМОМ В ДЕЙСТВИЯХ И НАБЛЮДЕНИЯХ
# env = Wrapper(BWg(), apr, action_repeat=3)
# model_name = "wrapper_env_трава"
# buffer_name = "wrapper_env_трава.npz"

# ОБУЧЕНИЕ НА ТЕКСТОВОЙ ИГРЕ
env_name = "CartPole-v0"
env = gym.make(env_name)
model_name = f"data/{env_name}"
buffer_name = f"data/{env_name}.npz"


# SAC 2019 Params
gamma = 0.99
tau = 0.01
alpha = 0.2
a_lr = 3e-4
q_lr = 1e-3
p_lr = 1e-3
buffer_maxlen = 2000


def max_action_preprocessor(action):
    # for CartPole
    action = numpy.argmax(action)
    return action

# def rescale_action(agent, action):
#         return action * (agent.action_range[1] - agent.action_range[0]) / 2.0 +\
#             (agent.action_range[1] + agent.action_range[0]) / 2.0

def train(max_episodes, max_steps, batch_size):
    # agent = SACAgent(env, gamma, tau, alpha, q_lr, p_lr, a_lr, buffer_maxlen)
    agent = DQNAgent(env)
    replay_buffer_a = Buffer(buffer_maxlen)

    # Если необходимо загрузить предобученную модель
    # agent.load_model(name="pitmodel")

    # Если необходимо загрузить существующий буффер
    # replay_buffer_a = Buffer(buffer_maxlen, load_from="ямы.npz")

    rewards_log = []
    step_log = []
    best_score = -100
    steps = 1500
    episode = 0
    total_step = 0
    total_reward = 0
    TARGET_UPDATE = 10

    # for episode in range(max_episodes):
    while total_step < max_total_steps:
        state = env.reset()
        episode_reward = 0

        # steps += (max_steps - 50)/max_episodes
        # steps = int(steps)
        for step_in in range(1000):
            env.render()
            action = agent.get_action(state)
            # print(action)
            # exit()
            
            if numpy.random.randn() < agent.epsilon:
                agent.epsilon *= agent.epsilon_decay
                act = env.action_space.sample()
            else:
                act = numpy.argmax(action)

            next_state, reward, done, _ = env.step(act)
            replay_buffer_a.push(state, action, reward, next_state, done)

            episode_reward += reward
            total_reward += reward
            rewards_log += [total_reward]
            step_log += [total_step]
            total_step += 1

            if len(replay_buffer_a) > batch_size:
                states_a, actions_a, rewards_a, next_states_a, dones_a = replay_buffer_a.sample(batch_size)
                agent.update(states_a, actions_a, rewards_a, next_states_a, dones_a)

            # if done or step_in == max_steps-1:
            if done:
                # print(f"Episode: {episode}, reward: {episode_reward}, buffer_len: {len(replay_buffer_a.buffer)}, episode steps: {steps}, epsilon: {agent.epsilon}")
                print(f"Episode: {episode}, reward: {episode_reward}, buffer_len: {len(replay_buffer_a.buffer)}, episode steps: {steps}")
                episode += 1
                break

            state = next_state

        if episode_reward > best_score:
            print('-----------------')
            print(f'Model saved at score: {episode_reward}')
            print('-----------------')
            best_score = episode_reward
            replay_buffer_a.save(buffer_name)
            agent.save_model(name=model_name)

        # Update the target network, copying all weights and biases in DQN
        if episode % TARGET_UPDATE == 0:
            print("target net updated")
            agent.update_target_net()

    replay_buffer_a.save(buffer_name)
    return rewards_log, step_log


import utils
max_episodes = 300
train_count = 1
max_total_steps = 22000

all_rewards, all_steps = utils.train_series(train_count, max_total_steps, train)

# numpy.save("reports/pits_realenv_totalepisode", all_rewards)
# all_rewards = numpy.load("reports/pits_realenv_totalepisode.npy")


utils.plot_rl_statisctic(all_rewards, max_total_steps)
""" Скрипт для обучения агента и параллельного обучения VAE на одном типе преград.
VAE эмулирует буфер агента для последующей смешивания с другим агентом.
"""


from sac_policy import SACAgent
import gym
from env_wrapper import Wrapper, BWpit, RepeatWrapper, BWg, BWstu, BWstapit, BWsta
from buffer import Buffer
from aparam import aparam
from vae import Vae
import vae_config as config
apr = aparam()
import numpy


# SAC 2019 Params
gamma = 0.99
tau = 0.01
alpha = 0.2
a_lr = 3e-4
q_lr = 1e-3
p_lr = 1e-3
buffer_maxlen = 20000

def convert_buffer(buffer):
    l = len(buffer)
    numpy_buffer = numpy.zeros((l, 54))
    for i in range(l):
        numpy_buffer[i, :24] = buffer[i][0]

        # next state
        numpy_buffer[i, 24:48] = buffer[i][3]

        # action
        numpy_buffer[i, 48:52] = buffer[i][1]

        # reward
        numpy_buffer[i, 52] = buffer[i][2]

    return numpy_buffer

vae = Vae(config)

replay_buffer_a = Buffer(buffer_maxlen, load_from="data/pits/ямы.npz")
buffer = convert_buffer(replay_buffer_a.buffer)
vae.fit(buffer)

import matplotlib.pyplot as plt
def show(true, pred):
    for sb, sv in zip(true, pred):
        # yb = states_a[ii, :48]
        # yv = states_a[ii, :48]
        x = numpy.arange(len(sv))
        plt.scatter(x, sb)
        plt.scatter(x, sv)
        plt.show()

# TESTING
states_buffer, actions_buffer, rewards_buffer, next_states_a, dones_a = replay_buffer_a.sample(batch_size=3)
states_vae, actions_vae, rewards_vae, next_states_a, dones_a = vae.get_data(minimum=9.919896084070205, maximum=23.242458453377182, batch_size=3)

# show(states_buffer, states_vae)
# show(actions_buffer, actions_vae)
# show(rewards_buffer, rewards_vae)
# exit()


vae.save("data/ямы_vae.h5")


vae = Vae(config)
replay_buffer_a = Buffer(buffer_maxlen, load_from="data/stairs/лестницы.npz")
buffer = convert_buffer(replay_buffer_a.buffer)
vae.fit(buffer)
vae.save("data/лестницы_vae.h5")


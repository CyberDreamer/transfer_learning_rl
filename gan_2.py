# example of a dcgan on cifar10
import numpy
from numpy import expand_dims
from numpy import zeros
from numpy import ones
from numpy import vstack
from numpy.random import randn
from numpy.random import randint
from keras.datasets.cifar10 import load_data
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Reshape
from keras.layers import Flatten
from keras.layers import Conv2D
from keras.layers import Conv2DTranspose
from keras.layers import LeakyReLU
from keras.layers import Dropout
from matplotlib import pyplot
from buffer import Buffer
from keras import backend


# size of the latent space
latent_dim = 10



def wasserstein_loss(y_true, y_pred):
    return backend.mean(y_true * y_pred)

# implementation of wasserstein loss
def d_loss(d_real_logit, d_fake_logit):
    return backend.mean(d_real_logit - d_fake_logit)

def g_loss(d_real_logit, d_fake_logit):
    return backend.mean(d_fake_logit)

# define the standalone discriminator model
def define_discriminator(in_shape=(53,)):
    model = Sequential()
    model.add(Dense(64, activation='relu', input_shape=in_shape))
    model.add(Dense(16, activation='relu'))
    # model.add(Dense(4, activation='relu'))
    # model.add(Dense(4, activation='relu'))
    # model.add(Dropout(0.25))
    # model.add(Dense(1, activation='sigmoid'))
    model.add(Dense(1))
    # compile model
    opt = Adam(lr=0.0002, beta_1=0.5)
    # opt = Adam(lr=0.0001)
    model.compile(loss=wasserstein_loss, optimizer=opt, metrics=['accuracy'])
    return model

class Generator():
    def __init__(self):
        super().__init__()
        self.model = Sequential()
        # foundation for 4x4 image
        # n_nodes = 256 * 4 * 4
        self.model.add(Dense(16, input_dim=latent_dim, activation='relu'))
        # model.add(LeakyReLU(alpha=0.2))
        # model.add(Reshape((4, 4, 256)))
        # self.model.add(Dense(4, activation='relu'))
        self.model.add(Dense(64, activation='relu'))
        # self.model.add(Dense(32, activation='relu'))
        self.model.add(Dense(53,  activation='relu'))

    def get_data(self, min=11.97225511024395, max=25.717040805717208, batch_size=128):
        z_sample = numpy.random.randn(batch_size, latent_dim)
        X_batch = self.model.predict(z_sample)
        # print(X_batch.shape)
        # X_batch = reverse_normalize_vae(X_batch, self.minimums, self.maximums)[0]
        X_batch = X_batch * max - min - 1
        obs1 = X_batch[:, :24]
        obs2 = X_batch[:, 24:48]
        acts = X_batch[:, 48:52]
        rews = X_batch[:, 52].reshape(-1, 1)
        d = numpy.zeros((batch_size))
        return obs1, acts, rews, obs2, d

    def save(self, name="gan_g.h5"):
        self.model.save_weights(name)

    def load(self, name="gan_g.h5"):
        self.model.load_weights(name)

# define the combined generator and discriminator model, for updating the generator
def define_gan(g_model, d_model):
    # make weights in the discriminator not trainable
    d_model.trainable = False
    # connect them
    model = Sequential()
    # add generator
    model.add(g_model)
    # add the discriminator
    model.add(d_model)
    # compile model
    opt = Adam(lr=0.0002, beta_1=0.5)
    # opt = Adam(lr=0.0001)
    model.compile(loss=wasserstein_loss, optimizer=opt)
    return model

# select real samples
def generate_real_samples(n_samples):
    # choose random instances
    ix = randint(0, replay_buffer_a.shape[0], n_samples)
    # retrieve selected images
    X = replay_buffer_a[ix]

    # states_a, actions_a, rewards_a, next_states_a, dones_a = replay_buffer_a.sample(batch_size=n_samples)

    # generate 'real' class labels (1)
    y = -ones((n_samples, 1))
    # y = d_model.predict(X)
    return X, y

# generate points in latent space as input for the generator
def generate_latent_points(latent_dim, n_samples):
    # generate points in the latent space
    x_input = randn(latent_dim * n_samples)
    # reshape into a batch of inputs for the network
    x_input = x_input.reshape(n_samples, latent_dim)
    return x_input

# use the generator to generate n fake examples, with class labels
def generate_fake_samples(g_model, latent_dim, n_samples):
    # generate points in latent space
    x_input = generate_latent_points(latent_dim, n_samples)
    # predict outputs
    X = g_model.predict(x_input)
    # create 'fake' class labels (0)
    y = ones((n_samples, 1))
    # y = d_model.predict(X)
    return X, y

# create and save a plot of generated images
def save_plot(examples, epoch, n=7):
    # scale from [-1,1] to [0,1]
    examples = (examples + 1) / 2.0
    # plot images
    for i in range(n * n):
        # define subplot
        pyplot.subplot(n, n, 1 + i)
        # turn off axis
        pyplot.axis('off')
        # plot raw pixel data
        pyplot.imshow(examples[i])
    # save plot to file
    filename = 'generated_plot_e%03d.png' % (epoch+1)
    pyplot.savefig(filename)
    pyplot.close()

# evaluate the discriminator, plot generated images, save generator model
def summarize_performance(epoch, g_model, d_model, latent_dim, n_samples=150):
    # prepare real samples
    X_real, y_real = generate_real_samples(n_samples)
    # evaluate discriminator on real examples
    _, acc_real = d_model.evaluate(X_real, y_real, verbose=0)
    # prepare fake examples
    x_fake, y_fake = generate_fake_samples(g_model, latent_dim, n_samples)
    # evaluate discriminator on fake examples
    _, acc_fake = d_model.evaluate(x_fake, y_fake, verbose=0)
    # summarize discriminator performance
    # print('>Accuracy real: %.0f%%, fake: %.0f%%' % (acc_real*100, acc_fake*100))
    print(f'Epoch: {epoch + 1}')
    # save plot
    # save_plot(x_fake, epoch)
    # save the generator model tile file
    # filename = 'generator_model_%03d.h5' % (epoch+1)
    g_model.save("data/gan_pits_gen.h5")

# train the generator and discriminator
def train(g_model, d_model, gan_model, latent_dim, n_epochs=40, n_batch=256):
    bat_per_epo = 756
    # bat_per_epo = 1024
    half_batch = int(n_batch / 2)
    # manually enumerate epochs
    for i in range(n_epochs):
        # enumerate batches over the training set
        for j in range(bat_per_epo):
            # get randomly selected 'real' samples
            X_real, y_real = generate_real_samples(half_batch)
            # print(X_real)
            # exit()
            # update discriminator model weights
            d_loss1, _ = d_model.train_on_batch(X_real, y_real)
            # generate 'fake' examples
            X_fake, y_fake = generate_fake_samples(g_model, latent_dim, half_batch)
            # print(X_fake)
            # exit()
            # update discriminator model weights
            d_loss2, _ = d_model.train_on_batch(X_fake, y_fake)
            # prepare points in latent space as input for the generator
            X_gan = generate_latent_points(latent_dim, n_batch)
            # create inverted labels for the fake samples
            y_gan = -ones((n_batch, 1))
            # update the generator via the discriminator's error
            g_loss = gan_model.train_on_batch(X_gan, y_gan)

            # Clip critic weights
            for l in d_model.layers:
                weights = l.get_weights()
                weights = [numpy.clip(w, -0.01, 0.01) for w in weights]
                l.set_weights(weights)
                    
            # summarize loss on this batch
            # print('>%d, %d/%d, d1=%.3f, d2=%.3f g=%.3f' %
            #     (i+1, j+1, bat_per_epo, d_loss1, d_loss2, g_loss))
        # evaluate the model performance, sometimes
        if (i+1) % 10 == 0:
            summarize_performance(i, g_model, d_model, latent_dim)

def convert_buffer(buffer):
    l = len(buffer)
    numpy_buffer = numpy.zeros((l, 53))
    for i in range(l):
        numpy_buffer[i, :24] = buffer[i][0]
        # next state
        numpy_buffer[i, 24:48] = buffer[i][3]
        # action
        numpy_buffer[i, 48:52] = buffer[i][1]
        # reward
        numpy_buffer[i, 52] = buffer[i][2]

    return numpy_buffer

def norm(data):
    min = abs(numpy.min(data)) + 1
    data += min
    max = numpy.max(data)
    data /= max

    print(f'min: {min}, max: {max}')
    return data

if __name__ == "__main__":
    buffer_maxlen = 100000
    replay_buffer_a = Buffer(buffer_maxlen, load_from="data/pits/ямы.npz")
    replay_buffer_a = convert_buffer(replay_buffer_a.buffer)
    replay_buffer_a = norm(replay_buffer_a)

    # create the discriminator
    d_model = define_discriminator()
    # create the generator
    g_model = Generator().model
    # g_model.load_weights("data/gan_pits_gen.h5")
    # create the gan
    gan_model = define_gan(g_model, d_model)
    # load image data
    # dataset = load_real_samples()
    # train model
    train(g_model, d_model, gan_model, latent_dim)
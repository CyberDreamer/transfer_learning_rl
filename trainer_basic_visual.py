"""
Учит базового агента (который умеет проходить только один вид преград)
и сохраняет буфер и обученные веса модели.
"""
from sac_visual_policy import SACAgent
from dqn_visual_policy import DQNAgent
import gym
from env_wrapper import Wrapper, BWpit, RepeatWrapper, BWg, BWstu, BWstapit, BWsta
from buffer import Buffer
from aparam import aparam
apr = aparam()
import cv2
import numpy
import time

# ОБУЧЕНИЕ НА ТРАВЕ НА БАЗОВОЙ СРЕДЕ OPEN AI БЕЗ ОБЕРТОК
# env = gym.make("BipedalWalker-v2")
# model_name = "source_env_трава"
# buffer_name = "source_env_трава.npz"


# ДЛЯ ОБЕРТОК
# env = BWg() - если нужна среда с травой
# env = BWpit() - если нужна среда с ямами
# env = BWstapit() - если нужна среда с и лестницами

# ОБУЧЕНИЕ НА ТРАВЕ НА ОБЕРТКЕ С ПОВТОРЕНИЕМ ДЕЙСТВИЙ БЕЗ ШУМА
# env = RepeatWrapper(BWg(), apr, action_repeat=3)
# model_name = "repeat_env_трава"
# buffer_name = "repeat_env_трава.npz"

# ОБУЧЕНИЕ НА ЯМАХ НА ОБЕРТКЕ С ПОВТОРЕНИЕМ ДЕЙСТВИЙ БЕЗ ШУМА
env = RepeatWrapper(BWpit(), apr, action_repeat=3)
model_name = "data/pits/ямы2"
buffer_name = "data/pits/ямы2.npz"

# ОБУЧЕНИЕ НА ЛЕСТНИЦАХ НА ОБЕРТКЕ С ПОВТОРЕНИЕМ ДЕЙСТВИЙ БЕЗ ШУМА
# env = RepeatWrapper(BWsta(), apr, action_repeat=3)
# model_name = "data/stairs/лестницы_temp"
# buffer_name = "data/stairs/лестницы_temp.npz"


# ОБУЧЕНИЕ НА ТРАВЕ НА ОБЕРТКЕ С ПОВТОРЕНИЕМ ДЕЙСТВИЙ С ШУМОМ В ДЕЙСТВИЯХ И НАБЛЮДЕНИЯХ
# env = Wrapper(BWg(), apr, action_repeat=3)
# model_name = "wrapper_env_трава"
# buffer_name = "wrapper_env_трава.npz"

# ОБУЧЕНИЕ НА ТЕКСТОВОЙ ИГРЕ
# env_name = "CartPole-v0"
# env_name = "Breakout-v4"
env_name = "BreakoutNoFrameskip-v4"
from environment import Environment
env = Environment('BreakoutNoFrameskip-v4', {}, atari_wrapper=True, test=True)
# env = gym.make(env_name)
model_name = f"data/{env_name}"
buffer_name = f"data/{env_name}.npz"


# SAC 2019 Params
gamma = 0.99
tau = 0.01
alpha = 0.2
a_lr = 3e-4
q_lr = 1e-3
p_lr = 1e-3
buffer_maxlen = 100000
W, H = 84, 84

ACTION_REPEAT_COUNT = 4

def max_action_preprocessor(action):
    # for CartPole
    action = numpy.argmax(action)
    return action

def state_processor(state):
    # for breakout
    # state = state[37:-15, 10:-10]
    state = state[36:-14, 6:-6]
    # cv2.imshow("WPWPPWWP", state)
    # cv2.waitKey(100)

    state = cv2.cvtColor(state, cv2.COLOR_BGR2GRAY)
    state = cv2.resize(state, (W, H), interpolation=cv2.INTER_NEAREST)
    # cv2.imshow("WPWPPWWP", state)
    # cv2.waitKey(10)
    # state = state.reshape(state.shape[2], state.shape[0], state.shape[1])
    state = numpy.expand_dims(state, axis=0)
    # state = state.reshape(1, state.shape[0], state.shape[1])
    return state

def action_processor(agent, qvals):
    # for jj in range(ACTION_REPEAT_COUNT):
    act = agent.select_action(qvals)

    state_pack = numpy.zeros((ACTION_REPEAT_COUNT, H, W), dtype=numpy.uint8)
    reward = 0
    for ii in range(ACTION_REPEAT_COUNT):
        # print(act)
        next_state, r, done, _ = env.step(act)
        state_pack[ii, :] = state_processor(next_state)[0]
        reward += r

    return state_pack, r, reward/1.0, done

def train():
    # agent = SACAgent(env, gamma, tau, alpha, q_lr, p_lr, a_lr, buffer_maxlen)
    agent = DQNAgent(env)
    replay_buffer_a = Buffer(buffer_maxlen)

    # Если необходимо загрузить предобученную модель
    # agent.load_model(name=f"data/{env_name}")

    # Если необходимо загрузить существующий буффер
    # replay_buffer_a = Buffer(buffer_maxlen, load_from=f"data/{env_name}.npz")

    rewards_log = []
    step_log = []
    best_score = -100
    steps = 0
    episode = 0
    total_step = 0
    total_reward = 0
    TARGET_UPDATE = 10
    MAX_EPISODE_STEPS = 1000
    MAX_TOTAL_STEPS = 5500000
    BATCH_SIZE = 32

    # for episode in range(max_episodes):
    while total_step < MAX_TOTAL_STEPS:
        start_state = env.reset()
        # print("env reset")
        
        state, _, reward, done = action_processor(agent, [0.25, 0.25, 0.25, 0.25])
        # print(state.shape)
        # exit()

        episode_reward = 0
        episode_loss = 1
        train_count = 1
        # t1 = time.time()
        for step_in in range(MAX_EPISODE_STEPS):
            # env.render()
            qvals = agent.get_action(state)
            # print(action)
            # exit()
            next_state, _, reward, done = action_processor(agent, qvals)
            # print("action_processor")
            replay_buffer_a.push(state, qvals, reward, next_state, done)

            episode_reward += reward
            total_reward += reward
            rewards_log += [total_reward]
            step_log += [total_step]
            total_step += ACTION_REPEAT_COUNT

            state = next_state
            # t2 = time.time()
            if len(replay_buffer_a) > 30000:
                states_a, actions_a, rewards_a, next_states_a, dones_a = replay_buffer_a.sample(BATCH_SIZE)
                episode_loss += agent.update(states_a, actions_a, rewards_a, next_states_a, dones_a)
                train_count += 1
            # print(f"net update time: {time.time() - t2}")
            # if done or step_in == max_steps-1:
            if done:
                # print(f"Episode: {episode}, reward: {episode_reward}, buffer_len: {len(replay_buffer_a.buffer)}, episode steps: {steps}, epsilon: {agent.epsilon}")
                # if len(replay_buffer_a) > 30000:
                #     for kk in range(10):
                #         states_a, actions_a, rewards_a, next_states_a, dones_a = replay_buffer_a.sample(BATCH_SIZE)
                #         episode_loss = agent.update(states_a, actions_a, rewards_a, next_states_a, dones_a)

                # print("agent.update")
                print(f"total_step: {total_step}, reward: {episode_reward}, buffer_len: {len(replay_buffer_a.buffer)}, episode steps: {ACTION_REPEAT_COUNT*step_in}, epsilon: {round(agent.epsilon, 3)}, loss: {episode_loss/train_count}")
                episode += 1
                break

        # print(f"step time: {time.time() - t1}")

        if episode_reward > best_score:
            print('-----------------')
            print(f'! Achieve best score score: {episode_reward}')
            print('-----------------')
            best_score = episode_reward


        # Update the target network, copying all weights and biases in DQN
        if episode % TARGET_UPDATE == 0:
            print("target net updated and saved")
            # agent.update_target_net()
            # replay_buffer_a.save(buffer_name)
            # agent.save_model(name=model_name)

    replay_buffer_a.save(buffer_name)
    return rewards_log, step_log


import utils
max_episodes = 300
train_count = 1
max_total_steps = 22000

train()
exit()

all_rewards, all_steps = utils.train_series(train_count, max_total_steps, train)

# numpy.save("reports/pits_realenv_totalepisode", all_rewards)
# all_rewards = numpy.load("reports/pits_realenv_totalepisode.npy")


utils.plot_rl_statisctic(all_rewards, max_total_steps)
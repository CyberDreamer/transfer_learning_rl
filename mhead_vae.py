from keras.layers import Lambda, Input, Dense, Concatenate
from keras.models import Model
from keras.optimizers import Adam
from keras.losses import binary_crossentropy, mse
from keras import regularizers
from keras import backend as K
from keras.callbacks import EarlyStopping
import tensorflow as tf
import keras
# from utils import normalize_vae, reverse_normalize_vae

import numpy as np

class Vae:
    def __init__(self, config):
        self.config = config
        self.models, loss = self.create_vae()
        self.vae = self.models['vae']
        opt = Adam(learning_rate=0.001)
        self.vae.compile(optimizer=opt, loss=loss)
        self.encoder = self.models['encoder']
        self.decoder = self.models['decoder']

    def sampling(self, args):
        """Reparameterization trick by sampling from an isotropic unit Gaussian.
        # Arguments
            args (tensor): mean and log of variance of Q(z|X)
        # Returns
            z (tensor): sampled latent vector
        """

        z_mean, z_log_var = args
        batch = K.shape(z_mean)[0]
        dim = K.int_shape(z_mean)[1]
        # by default, random_normal has mean = 0 and std = 1.0
        epsilon = K.random_normal(shape=(batch, dim))
        return z_mean + K.exp(0.5 * z_log_var) * epsilon


    def create_vae(self):
        original_dim = self.config.ORIGINAL_DIM
        latent_dim = self.config.LATENT_DIM
        structure_encoder = self.config.STRUCTURE_ENCODER
        structure_decoder = self.config.STRUCTURE_DECODER
        models = {}
        input_state = Input(shape=(28,), name='input_state')
        input_nstate = Input(shape=(20,), name='input_nstate')
        input_action = Input(shape=(4,), name='input_action')
        input_reward = Input(shape=(1,), name='input_reward')
        # Убрали регуляризацию, т.к. хотели "словить" хотя бы переобучение. В принципе ничего не мешает вернуть регуляризацию
        # Добавили больше слоев, чтобы находить более сложные признаки
        xa1 = Dense(32,activation='relu')(input_state)
        xb1 = Dense(32,activation='relu')(input_nstate)
        xc1 = Dense(16,activation='relu')(input_action)
        xd1 = Dense(4,activation='relu')(input_reward)

        xa2 = Dense(64,activation='relu')(xa1)
        xb2 = Dense(64,activation='relu')(xb1)
        xc2 = Dense(32,activation='relu')(xc1)
        xd2 = Dense(8,activation='relu')(xd1)

        combined = Concatenate(axis=1)([xa2, xb2, xc2, xd2])
        # x2 = Dense(128,activation='relu')(x1)
        # x3 = Dense(128,activation='relu')(x2)
        z1 = Dense(128, activation='relu')(combined)
        z2 = Dense(128, activation='relu')(z1)
        z3 = Dense(256, activation='relu')(z2)
        z4 = Dense(256, activation='relu')(z3)
        z_mean = Dense(latent_dim, name='z_mean')(z4)
        z_log_var = Dense(latent_dim, name='z_log_var')(z4)

        z = Lambda(self.sampling, output_shape=(latent_dim,), name='z')([z_mean, z_log_var])

        # instantiate encoder model
        encoder = Model([input_state, input_nstate, input_action, input_reward], [z_mean, z_log_var, z], name='encoder')
        models["encoder"] = encoder

        # build decoder model
        latent_inputs = Input(shape=(latent_dim,), name='z_sampling')
        # Добавили больше слоев, чтобы находить более сложные признаки
        h1 = Dense(256, activation='relu')(latent_inputs)
        # x2 = Dense(128, activation='relu')(x1)
        # x3 = Dense(128, activation='relu')(x2)
        h2 = Dense(256, activation='relu')(h1)

        ya1 = Dense(128, activation='relu')(h2)
        yb1 = Dense(128, activation='relu')(h2)
        yc1 = Dense(32, activation='relu')(h2)
        yd1 = Dense(16, activation='relu')(h2)

        # сменили функцию активации с sigmoid на relu, так ошибка стала падать сильнее
        output_state = Dense(28, activation='relu')(ya1)
        output_nstate = Dense(20, activation='relu')(yb1)
        output_action = Dense(4, activation='relu')(yc1)
        output_reward = Dense(1, activation='relu')(yd1)

        # instantiate decoder model
        models["decoder"] = Model(latent_inputs, [output_state, output_nstate, output_action, output_reward], name='decoder')

        # instantiate VAE model
        outputs = models["decoder"](encoder([input_state, input_nstate, input_action, input_reward])[2])
        models["vae"] = Model([input_state, input_nstate, input_action, input_reward], outputs, name='vae')
        
        # def vae_loss(y_true,y_pred):
        #     # изменили аргументы loss функции с inputs, outputs на локальные значения в батче y_true, y_pred
        #     reconstruction_loss = binary_crossentropy(y_true,y_pred)
        #     # reconstruction_loss *= original_dim
        #     kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
        #     kl_loss = K.sum(kl_loss, axis=-1)
        #     kl_loss *= -0.5
        #     return K.mean(reconstruction_loss + kl_loss)

        def vae_loss(y_true, y_pred):
            reconstruction_loss = tf.reduce_mean(
                keras.losses.binary_crossentropy(y_true, y_pred)
            )
            reconstruction_loss *= original_dim
            kl_loss = 1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var)
            kl_loss = tf.reduce_mean(kl_loss)
            kl_loss *= -0.5
            total_loss = reconstruction_loss + kl_loss
            # total_loss = reconstruction_loss
            return total_loss

        return models, vae_loss

    def norm(self, data):
        min = abs(np.min(data)) + 1
        data += min
        max = np.max(data)
        data /= max

        print(f'min: {min}, max: {max}')
        return data, min, max

    def unnorm(self, data, min, max):
        return data * max - min - 1

    def fit(self, states, nstates, actions, rewards):
        # x_train, self.minimums, self.maximums = normalize_vae(x_train.copy())

        states, min, max = self.norm(states)
        nstates, min, max = self.norm(nstates)
        actions, min, max = self.norm(actions)
        rewards, min, max = self.norm(rewards)
        # exit()

        early_stopping = EarlyStopping(monitor='val_loss', patience=10)
        self.vae.fit([states, nstates, actions, rewards], [states, nstates, actions, rewards], epochs=self.config.EPOCHS, batch_size=self.config.BATCH_SIZE,
                     validation_split=self.config.VALIDATION_SPLIT, callbacks=[], shuffle=True)
        # self.vae.fit(x_train, x_train, epochs=self.config.EPOCHS, batch_size=self.config.BATCH_SIZE,
        #              validation_split=self.config.VALIDATION_SPLIT)

    def save(self, name="model_vae.h5"):
        self.vae.save_weights(name)

    def load(self, name="model_vae.h5"):
        self.vae.load_weights(name)

    def get_data(self, min, max, batch_size=128):
        z_sample = np.random.randn(batch_size, self.config.LATENT_DIM)
        states, nstates, actions, rewards = self.decoder.predict(z_sample)
        
        states = self.unnorm(states, min[0], max[0])
        nstates = self.unnorm(nstates, min[1], max[1])
        actions = self.unnorm(actions, min[2], max[2])
        rewards = self.unnorm(rewards, min[3], max[3])

        # X_batch = reverse_normalize_vae(X_batch, self.minimums, self.maximums)[0]
        # X_batch = X_batch * maximum - minimum - 1
        obs1 = np.zeros((states.shape[0], 24))
        obs2 = np.zeros((states.shape[0], 24))
        obs1[:, :14] = states[:, :14]
        obs1[:, 14:] = nstates[:, :10]
        obs2[:, :14] = states[:, 14:]
        obs2[:, 14:] = nstates[:, 10:]
        # acts = X_batch[:, 48:52]
        # rews = X_batch[:, 52].reshape(-1, 1)
        d = np.zeros((batch_size))
        return obs1, actions, rewards, obs2, d
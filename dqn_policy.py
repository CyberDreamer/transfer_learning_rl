import torch
import torch.nn as nn
import torch.autograd as autograd
import numpy as np
import torch.nn.functional as F

class DQN(nn.Module): 
    def __init__(self, input_dim, output_dim):
        super(DQN, self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        
        self.fc = nn.Sequential(
            nn.Linear(self.input_dim[0], 32),
            nn.ReLU(),
            nn.Linear(32, 32),
            nn.ReLU(),
            nn.Linear(32, 32),
            nn.ReLU(),
            nn.Linear(32, self.output_dim)
            # nn.ReLU()
        )

    def forward(self, state):
        qvals = torch.softmax(self.fc(state), dim=1)
        # qvals = self.fc(state)
        return qvals
    
class DQNAgent:
    def __init__(self, env, learning_rate=3e-4, gamma=0.99, buffer_size=10000, state_shape=(50,)):
        self.env = env
        self.learning_rate = learning_rate
        self.gamma = gamma

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        # self.device = 'cpu'
        old_shape = env.observation_space.shape
        # input_shape = (old_shape[2], old_shape[0], old_shape[1])
        input_shape = env.observation_space.shape[0]
        print('Create DQN agent with:')
        print(f'device: {self.device}')
        print(f'observation_space: {input_shape}')
        print(f'action_space: {env.action_space.n}')

        self.policy_net = DQN((input_shape,), env.action_space.n).to(self.device)
        self.target_net = DQN((input_shape,), env.action_space.n).to(self.device)
        self.target_net.load_state_dict(self.policy_net.state_dict())
        self.target_net.eval()

        self.optimizer = torch.optim.RMSprop(self.policy_net.parameters())

        # self.optimizer = torch.optim.Adam(self.policy_net.parameters(), lr=learning_rate)
        self.MSE_loss = nn.MSELoss()

        self.epsilon = 0.95
        # self.epsilon = 0.999999
        # self.epsilon_decay = 0.9995
        self.epsilon_decay = 0.9998
        self.min_epsilon = 0.05

    def get_action(self, state, eps=0.20):
        state = torch.FloatTensor(state).float().unsqueeze(0).to(self.device)
        qvals = self.policy_net.forward(state).cpu().detach().numpy()
        qvals = np.reshape(qvals, (2))
        # action = np.argmax(qvals)
        # print(qvals)
        # exit()
        # qvals = qvals.cpu().detach().numpy().reshape((2))
        # print(qvals.shape)
        # print(qvals)
        # return np.random.choice(len(qvals), 1, p=qvals)[0]

        if np.random.randn() < self.epsilon:
            self.epsilon *= self.epsilon_decay
            return self.env.action_space.sample()

        return qvals

    def compute_loss(self, states, actions, rewards, next_states, dones):
        # print(actions)
        # print(type(actions))
        curr_Q = self.policy_net.forward(states).gather(1, actions.unsqueeze(1))
        next_Q = self.target_net.forward(next_states)

        # greedy update (Q-learning)
        # next_Q = torch.max(next_Q, 1)[0]

        # epsilon greedy update (SARSA)
        next_Q = next_Q.cpu().detach().numpy()
        temp_Q = np.zeros(next_Q.shape[0])
        for i in range(next_Q.shape[0]):
            temp_Q[i] = np.random.choice(len(next_Q[i]), 1, p=next_Q[i])[0]

        next_Q = torch.FloatTensor(temp_Q).to(self.device)
        # print(next_states.shape)
        # print(next_Q.shape)
        # exit()

        expected_Q = rewards.squeeze(1) + self.gamma * next_Q

        # loss = self.MSE_loss(curr_Q, expected_Q)
        loss = F.smooth_l1_loss(curr_Q, expected_Q)
        return loss

    def update(self, states, actions, rewards, next_states, dones):
        states = torch.FloatTensor(states).to(self.device)
        states = (states + 3.0)/6.0
        # print(states)

        actions = torch.LongTensor(actions).to(self.device)
        rewards = torch.FloatTensor(rewards).to(self.device)
        next_states = torch.FloatTensor(next_states).to(self.device)
        next_states = (next_states + 3.0)/6.0

        dones = torch.FloatTensor(dones).to(self.device)
        dones = dones.view(dones.size(0), -1)

        loss = self.compute_loss(states, actions, rewards, next_states, dones)

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

    def update_target_net(self):
        self.target_net.load_state_dict(self.policy_net.state_dict())

    def save_model(self, name="model_q"):
        torch.save(self.policy_net.state_dict(), name + "_dqn.md5")
""" Скрипт для обучения агента на двух VAE без реального взаимодействия со средой вообще
"""

from sac_policy import SACAgent
import gym
import numpy
from env_wrapper import Wrapper, BWpit, RepeatWrapper, BWg, BWstu, BWstapit, BWsta
from buffer import Buffer
from aparam import aparam
apr = aparam()
# from mhead_vae import Vae
# import gan_2
import mhead_gan
import vae_config as config
import torch

# env = gym.make("Pendulum-v0")
# env = gym.make("BipedalWalker-v2")

# env = RepeatWrapper(BWstapit(), apr, 3)
# env = RepeatWrapper(BWsta(), apr, action_repeat=3)
env = RepeatWrapper(BWpit(), apr, action_repeat=3)

gamma = 0.99
tau = 0.01
alpha = 0.2
a_lr = 3e-4
q_lr = 1e-3
p_lr = 1e-3
buffer_maxlen = 50000


def rescale_action(agent, action):
        return action * (agent.action_range[1] - agent.action_range[0]) / 2.0 +\
            (agent.action_range[1] + agent.action_range[0]) / 2.0

def train(max_episodes=800, max_steps=500, batch_size=256):
    agent = SACAgent(env, gamma, tau, alpha, q_lr, p_lr, a_lr, buffer_maxlen)
    # agent.load_model(name="data/pits/ямы")


    gan_a = mhead_gan.Generator()
    gan_a.load("data/mheadgan_pits_gen.h5")
    # gan_a.eval()
    gan_b = mhead_gan.Generator()
    gan_b.load("data/mheadgan_pits_gen.h5")
    # gan_b.eval()

    # replay_buffer_a.load("ямы.npz")

    rewards_log = []
    step_log = []
    best_score = -100
    steps = 1500
    episode = 0
    total_step = 0
    total_reward = 0

    # for episode in range(max_episodes):
    while total_step < max_total_steps:
        state = env.reset()
        episode_reward = 0

        # for step in range(max_steps):
        for step_in in range(steps):
            env.render()
            action = agent.get_action(state)
            next_state, reward, done, _ = env.step(rescale_action(agent, action))
            # replay_buffer_b.push(state, action, reward, next_state, done)

            episode_reward += reward
            total_reward += reward
            rewards_log += [total_reward]
            step_log += [total_step]
            total_step += 1

            if done or step_in == max_steps-1:
                # episode_rewards.append(episode_reward)
                # print("Episode " + str(episode) + ": " + str(episode_reward))
                print(f"Episode: {episode}, reward: {episode_reward}, episode steps: {steps}, total_steps: {total_step}")
                episode += 1
                break

            state = next_state

            if total_step > batch_size:
                # states_a, actions_a, rewards_a, next_states_a, dones_a = vae_a.get_data(minimum=9.919896084070205, maximum=23.242458453377182, batch_size=batch_size)
                # states_b, actions_b, rewards_b, next_states_b, dones_b = vae_b.get_data(minimum=12.547492951750755, maximum=24.792445943802594, batch_size=batch_size)

                states_a, actions_a, rewards_a, next_states_a, dones_a = gan_a.get_data(batch_size=batch_size)
                states_b, actions_b, rewards_b, next_states_b, dones_b = gan_b.get_data(batch_size=batch_size)

                states = numpy.concatenate((states_a, states_b))
                actions = numpy.concatenate((actions_a, actions_b))
                rewards = numpy.concatenate((rewards_a, rewards_b))
                next_states = numpy.concatenate((next_states_a, next_states_b))
                dones = numpy.concatenate((dones_a, dones_b))

                agent.update(states, actions, rewards, next_states, dones)

        if episode_reward > best_score:
            print(f'Model saved at score: {episode_reward}')
            best_score = episode_reward
            # agent.save_model("data/vae_mix_stairs_pits/vae_mix")

    return rewards_log, step_log


# episode_rewards = train(env, agent, 800, 500, 256)


import utils
max_episodes = 300
train_count = 2
max_total_steps = 10000

all_rewards, all_steps = utils.train_series(train_count, max_total_steps, train)

# numpy.save("reports/pits_realenv_totalepisode", all_rewards)
# all_rewards = numpy.load("reports/pits_realenv_totalepisode.npy")


utils.plot_rl_statisctic(all_rewards, max_total_steps)
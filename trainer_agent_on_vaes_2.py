""" Скрипт для обучения агента на двух VAE без реального взаимодействия со средой вообще
"""

from sac_policy import SACAgent
import gym
import numpy
from env_wrapper import Wrapper, BWpit, RepeatWrapper, BWg, BWstu, BWstapit, BWsta
from buffer import Buffer
from aparam import aparam
apr = aparam()
# from mhead_vae import Vae
from vae import Vae
import vae_config as config

# env = gym.make("Pendulum-v0")
# env = gym.make("BipedalWalker-v2")

env = RepeatWrapper(BWstapit(), apr, 3)

# SAC 2019 Params
gamma = 0.99
tau = 0.01
alpha = 0.2
a_lr = 3e-4
q_lr = 1e-3
p_lr = 1e-3
buffer_maxlen = 50000

#2018 agent
#agent = SACAgent(env, gamma, tau, value_lr, q_lr, policy_lr, buffer_maxlen)

#2019 agent
agent = SACAgent(env, gamma, tau, alpha, q_lr, p_lr, a_lr, buffer_maxlen)
# agent.load_model(name="data/pits/ямы")


vae_a = Vae(config)
vae_a.load("data/ямы_mvae.h5")
vae_b = Vae(config)
vae_b.load("data/лестницы_mvae.h5")

# replay_buffer_a.load("ямы.npz")

def rescale_action(action):
        return action * (agent.action_range[1] - agent.action_range[0]) / 2.0 +\
            (agent.action_range[1] + agent.action_range[0]) / 2.0

def mini_batch_train(env, agent, max_episodes, max_steps, batch_size):
    episode_rewards = []
    best_score = -100

    for episode in range(max_episodes):
        state = env.reset()
        episode_reward = 0

        for step in range(max_steps):
            env.render()
            action = agent.get_action(state)
            next_state, reward, done, _ = env.step(rescale_action(action))
            # replay_buffer_b.push(state, action, reward, next_state, done)
            episode_reward += reward

            if done or step == max_steps-1:
                episode_rewards.append(episode_reward)
                print("Episode " + str(episode) + ": " + str(episode_reward))
                break

            state = next_state

        # states_a, actions_a, rewards_a, next_states_a, dones_a = vae_a.get_data(minimum=9.919896084070205, maximum=23.242458453377182, batch_size=batch_size)
        # states_b, actions_b, rewards_b, next_states_b, dones_b = vae_b.get_data(minimum=12.547492951750755, maximum=24.792445943802594, batch_size=batch_size)

        states_a, actions_a, rewards_a, next_states_a, dones_a = vae_a.get_data(min=[4.638041814168295, 1.0603353679180145, 2.0, 11.97225511024395],
                                                                                max=[8.281194051106771, 2.0603353679180145, 3.0, 25.717040805717208],
                                                                                batch_size=batch_size)
        states_b, actions_b, rewards_b, next_states_b, dones_b = vae_b.get_data(min=[5.351858139038086, 1.0636773258447647, 2.0, 11.115044891436902],
                                                                                max=[9.380502223968506, 2.0636773258447647, 3.0, 25.04369591226181],
                                                                                batch_size=batch_size)

        states = numpy.concatenate((states_a, states_b))
        actions = numpy.concatenate((actions_a, actions_b))
        rewards = numpy.concatenate((rewards_a, rewards_b))
        next_states = numpy.concatenate((next_states_a, next_states_b))
        dones = numpy.concatenate((dones_a, dones_b))

        agent.update(states, actions, rewards, next_states, dones)

        if episode_reward > best_score:
            print(f'Model saved at score: {episode_reward}')
            best_score = episode_reward
            agent.save_model("data/vae_mix_stairs_pits/vae_mix")

    return episode_rewards

episode_rewards = mini_batch_train(env, agent, 800, 500, 256)

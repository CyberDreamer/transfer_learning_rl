import numpy as np
import random
from collections import deque, namedtuple
import pickle

Experience = namedtuple('Experience', field_names=['state', 'policy_outputs', 'action', 'reward', 'last_state'])

class Buffer:
    def __init__(self, max_size, load_from=None):
        self.max_size = max_size

        if load_from is None:
            self.buffer = deque(maxlen=max_size)
        else:
            self.buffer = self.load(load_from)

    def push(self, state, action, reward, next_state, done):
        experience = (state, action, np.array([reward]), next_state, done)
        self.buffer.append(experience)

    def sample(self, batch_size):
        state_batch = []
        action_batch = []
        reward_batch = []
        next_state_batch = []
        done_batch = []

        batch = random.sample(self.buffer, batch_size)

        for experience in batch:
            state, action, reward, next_state, done = experience
            state_batch.append(state)
            action_batch.append(action)
            reward_batch.append(reward)
            next_state_batch.append(next_state)
            done_batch.append(done)

        return (state_batch, action_batch, reward_batch, next_state_batch, done_batch)

    def __len__(self):
        return len(self.buffer)

    def save(self, save_to):
        with open(save_to, 'wb') as f:
            pickle.dump(self.buffer, f)

    def load(self, load_from):
        with open(load_from, 'rb') as f:
            data = pickle.load(f)

        return data
"""
Учит агента на одном загруженном статичном буффере и одном реальном буффере
"""
from sac_policy import SACAgent
import gym
import numpy
from env_wrapper import Wrapper, BWpit, RepeatWrapper, BWg, BWstu, BWstapit, BWsta
from buffer import Buffer
from aparam import aparam
apr = aparam()

# env = gym.make("Pendulum-v0")
# env = gym.make("BipedalWalker-v2")

env = RepeatWrapper(BWstapit(), apr, 3)
model_name = "лестницы_ямы_buffer"


# SAC 2019 Params
gamma = 0.99
tau = 0.01
alpha = 0.2
a_lr = 3e-4
q_lr = 1e-3
p_lr = 1e-3
buffer_maxlen = 50000

#2018 agent
#agent = SACAgent(env, gamma, tau, value_lr, q_lr, policy_lr, buffer_maxlen)

#2019 agent
agent = SACAgent(env, gamma, tau, alpha, q_lr, p_lr, a_lr, buffer_maxlen)
agent.load_model(name="pitmodel")


replay_buffer_a = Buffer(buffer_maxlen, load_from="ямы.npz")
replay_buffer_b = Buffer(buffer_maxlen)

# replay_buffer_a.load("ямы.npz")

def rescale_action(action):
        return action * (agent.action_range[1] - agent.action_range[0]) / 2.0 +\
            (agent.action_range[1] + agent.action_range[0]) / 2.0

def mini_batch_train(env, agent, max_episodes, max_steps, batch_size):
    episode_rewards = []
    best_score = -100

    for episode in range(max_episodes):
        state = env.reset()
        episode_reward = 0

        for step in range(max_steps):
            env.render()
            action = agent.get_action(state)
            next_state, reward, done, _ = env.step(rescale_action(action))
            replay_buffer_b.push(state, action, reward, next_state, done)
            episode_reward += reward

            if len(replay_buffer_b.buffer) > batch_size:
                states_a, actions_a, rewards_a, next_states_a, dones_a = replay_buffer_a.sample(batch_size)
                states_b, actions_b, rewards_b, next_states_b, dones_b = replay_buffer_b.sample(batch_size)

                states = numpy.concatenate((states_a, states_b))
                actions = numpy.concatenate((actions_a, actions_b))
                rewards = numpy.concatenate((rewards_a, rewards_b))
                next_states = numpy.concatenate((next_states_a, next_states_b))
                dones = numpy.concatenate((dones_a, dones_b))

                # print(len(states_a))
                # print(states.shape)
                # exit()

                agent.update(states, actions, rewards, next_states, dones)

            if done or step == max_steps-1:
                episode_rewards.append(episode_reward)
                print("Episode " + str(episode) + ": " + str(episode_reward))
                break

            state = next_state

        if episode_reward > best_score:
            print(f'Model saved at score: {episode_reward}')
            best_score = episode_reward
            agent.save_model(model_name)

    return episode_rewards

episode_rewards = mini_batch_train(env, agent, 400, 3000, 128)
